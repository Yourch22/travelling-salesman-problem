#1.2 Problém obchodného cestujúceho Nearest insertion of arbitrary city
#- naprogramovať algoritmus pre riešenie problému obchodného cestujúceho
#v ľubovoľnom programovacom jazyku (najlepšie v Rku)
#-vstupom bude textový súbor, kde každý riadok bude predstavovať jednu hranu
#medzi uzlami (mestami) v grafe a jej váhu v tvare uzol medzera uzol medzera váha.
#1 2 0.8 
#-výstupom algoritmu bude textový súbor obsahujúci celkovú cenu cesty v prvom riadku
#a postupnosť uzlov (miest) ako ich obchodný cestujúci navštívil v druhom riadku
#( uzly budú oddelene čiarkami): 
#13 
#1,4,3,2...

#Naprogramoval : Jakub Jurč, FEI TUKE 2020.

import random

def najkratsie(mesto):
    first = 0
    riadok = 0
    druhe = 0
    for a in range(0, riadky):
        if pole[a][0] == mesto or pole[a][1] == mesto:
            if first == 0:
                min = pole[a][2]
                riadok = a
                first = 1
            if min > pole[a][2]:
                min = pole[a][2]
                riadok = a
    if pole[riadok][0] == mesto:
        druhe = pole[riadok][1]
    if pole[riadok][1] == mesto:
        druhe = pole[riadok][0]
    return druhe

def vklad(zozn, mesto, predch):
    novyzoz = []
    for b in range(0, (len(zozn))):
        novyzoz.append(zozn[b])
        if zozn[b] == predch:
            index = b+1
            break
    novyzoz.append(mesto)
    for c in range(index, (len(zozn))):
        novyzoz.append(zozn[c])
    return novyzoz

def generuj(zozn):
    num = random.randrange(0, riadky)
    while(pole[num][0] in zozn):
        num = random.randrange(0, riadky)
    return num

def count(array):
    pomoc = [0]
    pomoc[0] = array[0][0]
    for d in range(0, len(array)):
        for e in range(0, (len(pomoc))):
            podm = 0
            for f in range(0, (len(pomoc))):
                if pomoc[f]== array[d][0] :
                    podm = 1
            if pomoc[e] != array[d][0] :
                if podm == 0:
                    pomoc.append(array[d][0])
            podm = 0
            for f in range(0, (len(pomoc))):
                if pomoc[f] == array[d][1] :
                    podm = 1
            if pomoc[e] != array[d][1] :
                if podm == 0:
                    pomoc.append(array[d][1])
    return len(pomoc)

def vzdialenost(array, a, b):
    for g in range(0, len(array)):
        if (pole[g][0] == a) & (pole[g][1] == b) :
            return pole[g][2]
        if (pole[g][0] == b) & (pole[g][1] == a) :
            return pole[g][2]

def sucet(zoz):
    sucet = 0
    for h in range (0, (len(zoz)-1)):
        sucet += int(vzdialenost(pole, zoz[h], zoz[h+1]))
    sucet += int(vzdialenost(pole, zoz[0], zoz[len(zoz)-1]))
    return sucet
    
fvstup = open("input.txt", "r")
riadky = sum(1 for line in fvstup)
fvstup.seek(0)
stlpce = 3
pole = [[0 for y in range(stlpce)] for z in range(riadky)] 
for x in range(0, riadky):
    pole[x][0] = fvstup.read(1)
    medzera = fvstup.read(1)
    pole[x][1] = fvstup.read(1)
    medzera = fvstup.read(1)
    pole[x][2] = fvstup.read(1)
    medzera = fvstup.read(1)
fvstup.close()
prve = random.randrange(0, (riadky - 1))
zoznam = [0]
zoznam[0] = pole[prve][0]
print(zoznam)
zoznam.append(najkratsie(zoznam[0]))
print(zoznam)
gen = generuj(zoznam)
k = pole[gen][0]
zoznam = vklad(zoznam, k, zoznam[0])
dlzka = 0
while dlzka != len(zoznam):
    gen = generuj(zoznam)
    k = pole[gen][0]
    print(zoznam)
    koefmin = 9999999
    koefi = None
    koefj = None
    for h in range(0, (len(zoznam)-2)):
        cik = vzdialenost(pole, zoznam[h], k)
        ckj = vzdialenost(pole, k, zoznam[h+1])
        cij = vzdialenost(pole, zoznam[h], zoznam[h+1])
        koef = int(cik) + int(ckj) - int(cij)
        if koef < int(koefmin) :
            koefmin = koef
            koefi = zoznam[h]
            koefj = zoznam[h+1]
    zoznam = vklad(zoznam, k, koefi)
    dlzka = count(pole)
final = sucet(zoznam)
fvystup = open("output.txt", "w")
fvystup.write(str(final))
fvystup.write(" \n")
for i in range(0, len(zoznam)):
    fvystup.write(zoznam[i])
    fvystup.write(", ")
fvystup.close()
print(zoznam)
print(final)
